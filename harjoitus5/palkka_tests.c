#include "palkka_tests.h"
#include "palkka.h"
#include <stdio.h>

void run_tests()
{
	polku1();
	polku2();
	polku3();
	polku4();
	polku5();
	polku6();
}

/* kk_palkka virhe */
void polku1()
{
	if (laske_joulukuun_palkka(1400, 2) != -1)
		fprintf(stderr, "virhe polku1\n");
}

/* palvelsuaika virhe */
void polku2()
{
	if (laske_joulukuun_palkka(1500, -1) != -1)
		fprintf(stderr, "virhe polku2\n");
}

/* palvelusaika > 8 */
void polku3()
{
	if (laske_joulukuun_palkka(1500, 9) != 1500)
		fprintf(stderr, "virhe polku3\n");
}

/* palvelusaika > 5 */
void polku4()
{
	if (laske_joulukuun_palkka(1500, 6) != 1125)
		fprintf(stderr, "virhe polku4\n");
}

/* palvelusaika >= 3 */
void polku5()
{
       	if (laske_joulukuun_palkka(1500, 3) != 750)
		fprintf(stderr, "virhe polku5\n");
}

/* palvelusaika < 3 */
void polku6()
{
	if (laske_joulukuun_palkka(1500, 2) != 0)
		fprintf(stderr, "virhe polku6\n");
}
