#include <stdio.h>
#include <stdlib.h>
#include "palkka.h"
#include "palkka_tests.h"

int main()
{
	run_tests();
	return 0;
}

/* Käydään läpi ei-käyvät ekvivalenssiluokat. */
int tarkista_syote(double kk_palkka, int palvelusaika)
{
	/* ekv. luokat 2, 3 */
	if (kk_palkka < 1500 || kk_palkka > 3500) {
		return 0;
	}
	/* ekv. luokat 4, 5 */
	if (palvelusaika < 0 || palvelusaika > 50) {
		return 0;
	}
	return 1;
}

double laske_joulukuun_palkka(double kk_palkka, int palvelusaika)
{
	if (!tarkista_syote(kk_palkka, palvelusaika)) {
		return -1;
	}
	if (palvelusaika > 8)
		return kk_palkka * 1.0;
	else if (palvelusaika > 5)
		return kk_palkka * 0.75;
	else if (palvelusaika >= 3)
		return kk_palkka * 0.50;
	return 0.0;
}
